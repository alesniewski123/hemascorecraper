from bs4 import BeautifulSoup

with open("../tmp/sample_matches.html", 'r', encoding='utf-8') as f:
  soup = BeautifulSoup(f, 'html.parser')

results = soup.find(id='page-wrapper')
pools = results.find_all('div', class_='grid-margin-x')
for pool_nr in range(len(pools)):
    print("Pool", pool_nr+1)
    matches = pools[pool_nr].find_all('div', class_='match-item')
    for matchNr in range(len(matches)):
        match = matches[matchNr]
        matchId = match.find('input', {'name': 'matchID'}).get('value')
        matchFighter1 = match.find("li", class_='fighter_1_color').find_all(["h5", "h6"])
        matchFighter1Name = matchFighter1[0].text.strip()
        matchFighter1Score = matchFighter1[1].text.strip()
        matchFighter2 = match.find("li", class_='fighter_2_color').find_all(["h5", "h6"])
        matchFighter2Name = matchFighter2[0].text.strip()
        matchFighter2Score = matchFighter2[1].text.strip()
        print(matchNr + 1, "-", matchId)
        print(matchFighter1Name, "vs", matchFighter2Name)
        print(matchFighter1Score, ":", matchFighter2Score)


# with open("./tmp/sample_matches.html", "wb") as f:
#   f.write(pageChangeTournament.content)
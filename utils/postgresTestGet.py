import psycopg2
from psycopg2.extras import RealDictCursor

con = psycopg2.connect(database="hemacorecard",
                       user="postgres",
                       password="admin",
                       host="127.0.0.1",
                       port="5432",
                       cursor_factory=RealDictCursor)
print("Database opened successfully")

cur = con.cursor()
cur.execute("SELECT * from public.tournaments")
rows = cur.fetchall()

for row in rows:
    print("col 1 =", row['id'])
    print("col 2 =", row['event_id'])
    print("col 3 =", row['name'], "\n")

print("Operation done successfully")
con.close()
from bs4 import BeautifulSoup

with open("../tmp/sample_tournaments.html", 'r', encoding='utf-8') as f:
  soup = BeautifulSoup(f, 'html.parser')

results = soup.find(id='tourney-animated-menu')
tournaments = results.find_all("form")
tournamentsLength = len(tournaments)
for tournamentNr in range(tournamentsLength):
  tournament = tournaments[tournamentNr]
  tournamentId = tournament.find('input', {'name': 'newTournament'}).get('value')
  tournamentName = tournament.find("a").text.strip()
  print(tournamentId)
  print(tournamentName)

# print(tournaments_details)
# with open("./tmp/sample_tournaments.html", "wb") as f:
#     f.write(tournaments_to_extract.content)

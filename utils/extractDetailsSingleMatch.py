from bs4 import BeautifulSoup
with open("../tmp/sample_match.html", 'r', encoding='utf-8') as f:
  soup = BeautifulSoup(f, 'html.parser')

results = soup.find(id='page-wrapper')
fighter_score_boxes = results.find_all('div', class_='fighter-score-box')
elements_grid_padding_x = results.find_all('div', class_='grid-padding-x')
if len(elements_grid_padding_x) == 1:
  print("No")

scores_table = results.find_all('div', class_='grid-padding-x')[1]

# Fighter 1
fighter_1_score_box_spans = fighter_score_boxes[0].find_all('span')
fighter_1_name = fighter_1_score_box_spans[0].text.strip()
fighter_1_club = fighter_1_score_box_spans[1].text.strip()
fighter_1_score = fighter_1_score_box_spans[3].text.strip()

# Fighter 2
fighter_2_score_box_spans = fighter_score_boxes[1].find_all('span')
fighter_2_name = fighter_2_score_box_spans[0].text.strip()
fighter_2_club = fighter_2_score_box_spans[1].text.strip()
fighter_2_score = fighter_2_score_box_spans[3].text.strip()

print(fighter_1_name, "-", fighter_1_club, "vs", fighter_2_name, "-", fighter_2_club)
print(fighter_1_score, ":", fighter_2_score)

# Scores table
scores_table_rows = scores_table.find_all('div', class_='shrink')
rowsLength = len(scores_table_rows)
for rowNr in range(rowsLength-1):
  scores_table_row_cells = scores_table_rows[rowNr].find_all('div', class_='cell')
  cell_time = scores_table_row_cells[0].text.strip()
  cell_score_fighter_1 = scores_table_row_cells[1].text.strip()
  cell_score_fighter_2 = scores_table_row_cells[2].text.strip()
  print(rowNr, cell_time, cell_score_fighter_1, cell_score_fighter_2)

import requests

s = requests.Session()
URL = 'https://hemascorecard.com/poolMatches.php'

pageSelectEvent = s.post(URL, data={'formName': 'selectEvent',
                           'changeEventTo': 162})

pageChangeTournament = s.post(URL, data={'formName': 'changeTournament',
                           'newPage': 'participantsTournament.php',
                           'newTournament': 792})

pageGoToMatch = s.post(URL, data={'formName': 'goToMatch',
                           'matchID': 58338})

pageWithMatch = s.get("https://hemascorecard.com/scoreMatch.php")

with open("../tmp/sample_match.html", "wb") as f:
  f.write(pageWithMatch.content)
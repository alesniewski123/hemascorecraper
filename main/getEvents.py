import requests
from bs4 import BeautifulSoup
import csv


def get_raw_events_page(s):
    url = 'https://hemascorecard.com/infoSelect.php'
    page = s.get(url)
    return page


def extract_events_and_save_to_csv(raw_events_file, output_file):
    # Input file
    with open(raw_events_file, 'r', encoding='utf-8') as f:
        soup = BeautifulSoup(f, 'html.parser')
    # Output file
    writer = csv.writer(open(output_file, 'w',encoding='utf-8',newline=''))
    # Extracting
    results = soup.find(id='page-wrapper')
    event_years = results.find_all('div', class_='accordion-content')
    for year_nr in range(len(event_years)-2): # the last item is empty and 0 is for active events
        events_from_year = event_years[year_nr+1].find_all('button')
        for event_nr in range(len(events_from_year)):
            event = events_from_year[event_nr]
            event_id = event["value"]
            event_content = event.text.strip().replace("\n", ",")
            writer.writerow([event_id+","+event_content])
    print("Done")


# ------- Step 1 -------
# Init session
session = requests.Session()
# Get values
tournaments_to_extract = get_raw_events_page(session)
# Save output to HTML file
with open("../tmp/sample_events.html", "wb") as f:
    f.write(tournaments_to_extract.content)

# ------- Step 2 -------
# Extract events and save to CSV file
extract_events_and_save_to_csv("../tmp/sample_events.html", "./output/raw_events.csv")

import requests
from bs4 import BeautifulSoup
import psycopg2
from psycopg2.extras import RealDictCursor


def get_raw_matches_page(s, event_id, tournament_id):
    url = 'https://hemascorecard.com/poolMatches.php'

    s.post(url, data={'formName': 'selectEvent',
                          'changeEventTo': event_id})

    tournament_page = s.post(url, data={'formName': 'changeTournament',
                                        'newPage': 'poolMatches.php',
                                        'newTournament': tournament_id})
    return tournament_page


def extract_matches_details(soup_page):
    results = soup_page.find(id='page-wrapper')
    pools = results.find_all('div', class_='grid-margin-x')
    array_pools = []
    array_matches_ids = []
    array_fighter_1_names = []
    array_fighter_1_scores = []
    array_fighter_2_names = []
    array_fighter_2_scores = []
    for pool_nr in range(len(pools)):
        matches = pools[pool_nr].find_all('div', class_='match-item')
        for matchNr in range(len(matches)):
            match = matches[matchNr]
            match_id = match.find('input', {'name': 'matchID'}).get('value')
            match_fighter_1 = match.find("li", class_='fighter_1_color').find_all(["h5", "h6"])
            match_fighter_1_name = match_fighter_1[0].text.strip()
            match_fighter_1_score = match_fighter_1[1].text.strip()
            match_fighter_2 = match.find("li", class_='fighter_2_color').find_all(["h5", "h6"])
            match_fighter_2_name = match_fighter_2[0].text.strip()
            match_fighter_2_score = match_fighter_2[1].text.strip()

            array_pools.append(pool_nr + 1)
            array_matches_ids.append(match_id)
            array_fighter_1_names.append(match_fighter_1_name)
            array_fighter_1_scores.append(match_fighter_1_score)
            array_fighter_2_names.append(match_fighter_2_name)
            array_fighter_2_scores.append(match_fighter_2_score)

    return array_pools, array_matches_ids, array_fighter_1_names, array_fighter_1_scores, array_fighter_2_names, array_fighter_2_scores


session = requests.Session()
con = psycopg2.connect(database="hemacorecard",
                       user="postgres",
                       password="admin",
                       host="127.0.0.1",
                       port="5432",
                       cursor_factory=RealDictCursor)
cur = con.cursor()
cur.execute("SELECT * from public.tournaments")
tournaments = cur.fetchall()
for index, tournament in enumerate(tournaments):
    print(index, "/", len(tournaments), "/", round(index/len(tournaments), 2))
    print(tournament['name'])
    print(tournament['id'])
    matches_raw_page = get_raw_matches_page(session, tournament['event_id'], tournament['id'])
    soup = BeautifulSoup(matches_raw_page.content, 'html.parser')

    array_pools, array_matches_ids, array_fighter_1_names, array_fighter_1_scores, \
    array_fighter_2_names, array_fighter_2_scores = extract_matches_details(soup)

    for match_nr in range(len(array_matches_ids)):

        cur.execute("INSERT INTO matches (id,pool,fighter_name_1,fighter_score_1,fighter_name_2,fighter_score_2,tournament_id) VALUES ('" +
                str(array_matches_ids[match_nr]) + "', '" + str(array_pools[match_nr]) + "', '" +
                str(array_fighter_1_names[match_nr].replace("'", " ")) + "', '" + str(array_fighter_1_scores[match_nr]) + "', '" +
                str(array_fighter_2_names[match_nr].replace("'", " ")) + "', '" + str(array_fighter_2_scores[match_nr]) + "', " +
                str(tournament['id']) + ")")
        con.commit()

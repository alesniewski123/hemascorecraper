import requests
from bs4 import BeautifulSoup
import psycopg2
from psycopg2.extras import RealDictCursor


def get_raw_final_matches_page(s, event_id, tournament_id):
    url = 'https://hemascorecard.com/poolMatches.php'

    s.post(url, data={'formName': 'selectEvent',
                          'changeEventTo': event_id})

    s.post(url, data={'formName': 'changeTournament',
                                        'newPage': 'poolMatches.php',
                                        'newTournament': tournament_id})

    final_matches_page = s.post("https://hemascorecard.com/finalsBracket.php")
    return final_matches_page


def extract_final_matches_details(soup_page):
    results = soup_page.find(id='tournament_box')
    array_tiers = []
    array_matches_ids = []

    if results:
        tiers = results.find_all('div', class_='tier')
        for tier_nr in range(len(tiers)):
            tiers_name = tiers[tier_nr].find('h3').text.strip()
            matches = tiers[tier_nr].find_all('button', {'name': 'goToMatch'})
            for match_nr in range(len(matches)):
                match_id = matches[match_nr].get('value')
                array_tiers.append(tiers_name)
                array_matches_ids.append(match_id)

    return array_tiers, array_matches_ids


session = requests.Session()
con = psycopg2.connect(database="hemacorecard",
                       user="postgres",
                       password="admin",
                       host="127.0.0.1",
                       port="5432",
                       cursor_factory=RealDictCursor)
cur = con.cursor()
cur.execute("SELECT * from public.tournaments")
tournaments = cur.fetchall()
for index, tournament in enumerate(tournaments):
    print(index, "/", len(tournaments), "/", round(index/len(tournaments), 2))
    print(tournament['name'])
    print(tournament['id'])
    matches_raw_page = get_raw_final_matches_page(session, tournament['event_id'], tournament['id'])
    soup = BeautifulSoup(matches_raw_page.content, 'html.parser')
    array_tiers, array_matches_ids = extract_final_matches_details(soup)
    for match_nr in range(len(array_matches_ids)):
        cur.execute("INSERT INTO final_matches (id,tournament_id,tier_name) VALUES ('" +
                str(array_matches_ids[match_nr]) + "', '" + str(tournament['id']) + "', '" +
                str(array_tiers[match_nr].replace("'", " ")) + "')")
        con.commit()

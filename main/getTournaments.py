import requests
from bs4 import BeautifulSoup
import psycopg2
from psycopg2.extras import RealDictCursor


def get_raw_tournaments_page(s, event_id):
    url = 'https://hemascorecard.com/poolMatches.php'
    s.post(url, data={'formName': 'selectEvent',
                      'changeEventTo': event_id})
    page_info_summary = s.get("https://hemascorecard.com/infoSummary.php")
    return page_info_summary


def extract_tournaments_details(soup_page):
    results = soup_page.find(id='tourney-animated-menu')
    tournaments = results.find_all("form")
    array_tournament_ids = []
    array_tournament_names = []
    for tournament_nr in range(len(tournaments)):
        tournament = tournaments[tournament_nr]
        tournament_id = tournament.find('input', {'name': 'newTournament'}).get('value')
        tournament_name = tournament.find("a").text.strip()
        array_tournament_ids.append(tournament_id)
        array_tournament_names.append(tournament_name)
    return array_tournament_ids, array_tournament_names


session = requests.Session()
con = psycopg2.connect(database="hemacorecard",
                       user="postgres",
                       password="admin",
                       host="127.0.0.1",
                       port="5432",
                       cursor_factory=RealDictCursor)
cur = con.cursor()
cur.execute("SELECT * from public.events")
events = cur.fetchall()

for event in events:
    tournaments_raw_page = get_raw_tournaments_page(session, event['id'])
    soup = BeautifulSoup(tournaments_raw_page.content, 'html.parser')
    tournament_ids, tournament_names = extract_tournaments_details(soup)
    print(event['name'])
    for tournament_nr in range(len(tournament_ids)):
        cur.execute("INSERT INTO tournaments (id,event_id,name) VALUES (" + str(tournament_ids[tournament_nr]) + ", " + str(event['id']) + ", '" + tournament_names[tournament_nr].replace("'", " ") + "')")
        con.commit()
        print(tournament_names[tournament_nr])

con.close()
print("DONE")

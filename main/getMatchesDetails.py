import requests
from bs4 import BeautifulSoup
import psycopg2
from psycopg2.extras import RealDictCursor


def get_raw_matches_details_page(s, tournament_id, match_id):
    url = 'https://hemascorecard.com/poolMatches.php'
    s.post(url, data={'formName': 'goToMatch',
                                      'matchID': match_id})
    page_with_match = s.get("https://hemascorecard.com/scoreMatch.php")
    return page_with_match


def extract_match_details(soup_page):
    results = soup_page.find(id='page-wrapper')
    fighter_score_boxes = results.find_all('div', class_='fighter-score-box')

    # Fighter 1
    fighter_1_score_box_spans = fighter_score_boxes[0].find_all('span')
    fighter_1_name = fighter_1_score_box_spans[0].text.strip()
    fighter_1_club = fighter_1_score_box_spans[1].text.strip()
    fighter_1_score = fighter_1_score_box_spans[3].text.strip()

    # Fighter 2
    fighter_2_score_box_spans = fighter_score_boxes[1].find_all('span')
    fighter_2_name = fighter_2_score_box_spans[0].text.strip()
    fighter_2_club = fighter_2_score_box_spans[1].text.strip()
    fighter_2_score = fighter_2_score_box_spans[3].text.strip()

    # Scores table
    array_row_nrs = []
    array_cell_times = []
    array_cell_score_fighter_1 = []
    array_cell_score_fighter_2 = []

    elements_grid_padding_x = results.find_all('div', class_='grid-padding-x')
    if len(elements_grid_padding_x) == 1:
        return fighter_1_name, fighter_1_club, fighter_1_score, fighter_2_name, fighter_2_club, fighter_2_score, \
               array_row_nrs, array_cell_times, array_cell_score_fighter_1, array_cell_score_fighter_2
    scores_table = elements_grid_padding_x[1]
    scores_table_rows = scores_table.find_all('div', class_='shrink')

    for row_nr in range(len(scores_table_rows) - 1):
        scores_table_row_cells = scores_table_rows[row_nr].find_all('div', class_='cell')
        cell_time = scores_table_row_cells[0].text.strip()
        cell_score_fighter_1 = scores_table_row_cells[1].text.strip()
        cell_score_fighter_2 = scores_table_row_cells[2].text.strip()
        array_row_nrs.append(row_nr)
        array_cell_times.append(cell_time)
        array_cell_score_fighter_1.append(cell_score_fighter_1)
        array_cell_score_fighter_2.append(cell_score_fighter_2)

    return fighter_1_name, fighter_1_club, fighter_1_score, fighter_2_name, fighter_2_club, fighter_2_score, \
           array_row_nrs, array_cell_times, array_cell_score_fighter_1, array_cell_score_fighter_2

session = requests.Session()
url = 'https://hemascorecard.com/poolMatches.php'

session.post(url, data={'formName': 'selectEvent',
                  'changeEventTo': 162})

session.post(url, data={'formName': 'changeTournament',
                  'newPage': 'participantsTournament.php',
                  'newTournament': 792})

con = psycopg2.connect(database="hemacorecard",
                       user="postgres",
                       password="admin",
                       host="127.0.0.1",
                       port="5432",
                       cursor_factory=RealDictCursor)
cur = con.cursor()
cur.execute("SELECT * from public.matches")
matches = cur.fetchall()
for index, match in enumerate(matches):
    print(index, "/", len(matches), "/", round(index/len(matches), 2))
    print(match['id'])

    matches_raw_page = get_raw_matches_details_page(session, match['tournament_id'], match['id'])
    soup = BeautifulSoup(matches_raw_page.content, 'html.parser')
    fighter_1_name, fighter_1_club, fighter_1_score, fighter_2_name, fighter_2_club, fighter_2_score,\
    array_row_nrs, array_cell_times, array_cell_score_fighter_1, array_cell_score_fighter_2 = extract_match_details(soup)
    match_type = "Group"

    cur.execute("INSERT INTO match_details (id,fighter_name_1,fighter_score_1,fighter_club_1,fighter_name_2,fighter_score_2,fighter_club_2,tournament_id,type) VALUES ('" +
                str(match['id']) + "', '" +
                str(fighter_1_name.replace("'", " ")) + "', '" + str(fighter_1_score) + "', '" + str(fighter_1_club.replace("'", " ")) + "', '" +
                str(fighter_2_name.replace("'", " ")) + "', '" + str(fighter_2_score) + "', '" + str(fighter_2_club.replace("'", " ")) + "', " +
                str(match['tournament_id']) + ", '" + str(match_type) + "')")
    con.commit()

    for row_nr in range(len(array_row_nrs)):
        cur.execute("INSERT INTO points (match_id,nr,time,fighter_score_1,fighter_score_2) VALUES ('" +
                str(match['id']) + "', '" + str(array_row_nrs[row_nr]) + "', '" +
                str(array_cell_times[row_nr].replace("'", " ")) + "', '" + str(array_cell_score_fighter_1[row_nr]) + "', '" +
                str(array_cell_score_fighter_2[row_nr]) + "')")
        con.commit()
